package com.ibpd.shopping.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_express")
public class ExpressEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_code",length=45,nullable=true)
	private String code=null;
	@Column(name="f_tenantId",nullable=true)
	private Long tenantId=-1L;
	@Column(name="f_name",length=45,nullable=true)
	private String name=null;
	@Column(name="f_fee",nullable=true)
	private Double fee=0.0;
	/**
	 * 满多少钱免邮
	 */
	@Column(name="f_exempt",nullable=true)
	private Double exempt=0.0;
	@Column(name="f_order",length=1145,nullable=true)
	private Integer order=null;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order1) {
		this.order = order1;
	}
	/**
	 * 

insert  into "t_s_express"("f_code","f_name","f_fee","f_order") values ('EXPRESS','快递','5.00',2),('POST','平邮','0.00',1),('EMS','EMS','15.00',3),('expo','货运物流','0.00',NULL);


	 */
	public void setExempt(Double exempt) {
		this.exempt = exempt;
	}
	public Double getExempt() {
		return exempt;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public Long getTenantId() {
		return tenantId;
	}
}
