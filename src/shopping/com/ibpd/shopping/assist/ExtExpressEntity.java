package com.ibpd.shopping.assist;

import java.util.ArrayList;
import java.util.List;

import com.ibpd.shopping.entity.ExpressEntity;

public class ExtExpressEntity {

	private Double exemptPrice=0d;
	private Long tenantId=-1L;
	private String tenantName="";
	private List<ExpressEntity> rows=new ArrayList<ExpressEntity>();
	public ExtExpressEntity(ExpressEntity e){
		this.tenantId=e.getTenantId();
		this.setExemptPrice(e.getExempt());
	}
	public ExtExpressEntity() {
		// TODO Auto-generated constructor stub
	}
	public Double getExemptPrice() {
		return exemptPrice;
	}
	public void setExemptPrice(Double exemptPrice) {
		this.exemptPrice = exemptPrice;
	}
	public Long getTenantId() {
		return tenantId;
	}
	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}
	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public List<ExpressEntity> getRows() {
		return rows;
	}
	public void setRows(List<ExpressEntity> rows) {
		this.rows = rows;
	}
	
}
