package com.ibpd.shopping.assist;

import java.util.ArrayList;
import java.util.List;

import com.ibpd.shopping.entity.SpecEntity;

public class ExtProductSpecEntity {
	private List<SpecEntity> specList=new ArrayList<SpecEntity>();

	public void setSpecList(List<SpecEntity> specList) {
		this.specList = specList;
	}

	public List<SpecEntity> getSpecList() {
		return specList;
	}
}
