package com.ibpd.henuocms.service.nodeForm;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.NodeFormFieldEntity;
@Transactional
@Service("nodeFormFieldService")
public class NodeFormFieldServiceImpl extends BaseServiceImpl<NodeFormFieldEntity> implements INodeFormFieldService {
	public NodeFormFieldServiceImpl(){
		super();
		this.tableName="NodeFormFieldEntity";
		this.currentClass=NodeFormFieldEntity.class;
		this.initOK();
	}

} 
