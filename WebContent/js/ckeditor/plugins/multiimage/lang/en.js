﻿/**
 * 支持英文的语言包(文件名称en.js)，第一个参数是插件名称
 */
CKEDITOR.plugins.setLang('multiimage', 'en', {
	tbTip    : '',
	mytxt    : '',
	dlgTitle : '',
	editor.lang.mine : 'Multi Image Upload'
});